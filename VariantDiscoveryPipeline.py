#!/usr/bin/env python2.7
from lib import VariantAnalysisPipeline, parse_cmd


##################
# # MAIN
def main():
    args = parse_cmd()
    pipeline = VariantAnalysisPipeline(args)
    pipeline.setup()
    pipeline.start()

if __name__ == '__main__':
    main()
