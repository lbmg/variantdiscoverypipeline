from subprocess import call, Popen, PIPE
from os.path import join, exists, dirname, basename
from sys import exit
from os import remove
from ruffus.cmdline import MESSAGE
from ruffus.drmaa_wrapper import run_job, error_drmaa_job
from ClusterHandler import ClusterOptionsFactory


# recalibrate base qualities
def recalibrate_qualities(inputs, output_bam, JAVA, GATK, genome_fasta, nct, logger, logger_mutex, drmaa_session, cluster_drm):

    output_dir = dirname(output_bam)

    # file where the messages of the command
    stderr_file = open(output_bam.replace(".bam", ".stderr"), "a")

    stdout_res, stderr_res = "", ""

    # name of the job to be sent
    job1_name = "recal_"+basename(inputs[0])
    job2_name = "print_recal_"+basename(inputs[0])

    cmd1_array = [JAVA, "-jar",
                  GATK, "-T",
                  "BaseRecalibrator", "-R",
                  genome_fasta, "-I", inputs[0],
                  "-nct", str(nct), "-knownSites",
                  inputs[1], "-o",
                  output_bam.replace(".bam", "_report.grp")]

    cmd2_array = [JAVA, "-jar",
                  GATK, "-T",
                  "PrintReads", "-R",
                  genome_fasta, "-I", inputs[0],
                  "-nct", str(nct), "-BQSR",
                  output_bam.replace(".bam", "_report.grp"), "-o", output_bam]

    cmd1_clust = " ".join(cmd1_array)
    cmd2_clust = " " .join(cmd2_array)

    # check which job scheduler was setted and write the options
    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_nodes("1")
        options_object.set_ntasks(str(nct))
        run_local = False

        # cluster options for recalibration
        options_object.set_job_name(job1_name)
        other1_options = options_object.to_string()

        # cluster options to print recalibration
        options_object.set_job_name(job2_name)
        other2_options = options_object.to_string()

    else:
        other1_options = ""
        other2_options = ""
        run_local = True

    recal_quals = [cmd1_clust,
                   job1_name,
                   other1_options,
                   "Recalibrating qualities :\n\tSample\t\t: "+inputs[0]+"\n\tKnown sites\t: "+inputs[1],
                   "ERROR: there was a problem trying to recalibrate variants in sample "+inputs[0]]

    print_recal_quals = [cmd2_clust,
                         job2_name, other2_options,
                         "Printing recalibrated qualities of sample "+inputs[0],
                         "ERROR: there was a problem trying to print recalibrated qualities of sample "+inputs[0]]

    all_cmds = [recal_quals, print_recal_quals]

    for cmd in all_cmds:

        with logger_mutex:
            logger.log(MESSAGE, cmd[3])

        try:
            stdout_res, stderr_res = run_job(cmd_str=cmd[0],
                                             job_name=cmd[1],
                                             logger=logger,
                                             drmaa_session=drmaa_session,
                                             run_locally=run_local,
                                             job_script_directory=output_dir,
                                             job_other_options=cmd[2])
            for line in stderr_res:
                stderr_file.write(line)

        # ! remember that the exceptions are not being well throwed
        except error_drmaa_job as err:
            log_error = "\n".join(map(str, cmd[4], err))
            logger.log(MESSAGE, log_error)
            stderr_file.write(log_error)
            raise Exception(log_error)
            exit(1)


def build_hf_command(variant_type, input_vcf, JAVA, GATK, reference_fasta, filter_query, raw_extracted, filtered_output):

    filter_name = basename(input_vcf).replace(".vcf", "_"+variant_type+"_filter")

    cmd1 = [JAVA, "-jar",
            GATK, "-T",
            "SelectVariants", "-R",
            reference_fasta, "-V",
            input_vcf, "-selectType",
            variant_type, "-o", raw_extracted]

    cmd2 = [JAVA, "-jar",
            GATK, "-T",
            "VariantFiltration", "-R",
            reference_fasta, "-V",
            raw_extracted,
            "--filterExpression", "\""+filter_query+"\"",
            "--filterName", filter_name,
            "-o", filtered_output]

    cmd1_clust = " ".join(cmd1)
    cmd2_clust = " ".join(cmd2)

    return cmd1_clust, cmd2_clust


# filter variants
def hard_filtering(input_vcf, output_vcf, JAVA, GATK, reference_fasta, hf_snps, hf_indels, logger, drmaa_session, cluster_drm):

    # output file
    stderr_file = open(output_vcf.replace(".vcf", ".stdout"), "a")

    # variables where output will be stored
    stderr_res, stdout_res = "", ""

    output_dir = dirname(output_vcf)

    # define job names for all the tasks
    job1_snps, job2_snps = "extract_snps_"+input_vcf, "filter_snps_"+input_vcf
    job1_indels, job2_indels = "extract_indels_"+input_vcf, "filter_indels_"+input_vcf
    job_merge_filt = "merge_filtered_"+basename(input_vcf)

    # set command to run SNP extracting and filtering
    raw_extracted = output_vcf.replace("filtered.vcf", "raw_snps_only.vcf")
    filtered_snp_vcf = output_vcf.replace("filtered.vcf", "snps_filtered.vcf")

    cmd1_snps, cmd2_snps = build_hf_command("SNP",
                                            input_vcf,
                                            JAVA, GATK,
                                            reference_fasta,
                                            hf_snps,
                                            raw_extracted, filtered_snp_vcf)

    # set command to run INDEL extracting and filtering
    raw_extracted = output_vcf.replace("filtered.vcf", "raw_indels_only.vcf")
    filtered_indels_vcf = output_vcf.replace("filtered.vcf", "indels_filtered.vcf")

    cmd1_indels, cmd2_indels = build_hf_command("INDEL",
                                                input_vcf,
                                                JAVA, GATK,
                                                reference_fasta,
                                                hf_indels, raw_extracted,
                                                filtered_indels_vcf)

    cmd_joint_filt_array = [JAVA, "-jar",
                            GATK, "-T",
                            "CombineVariants", "-R",
                            reference_fasta, "--variant",
                            filtered_snp_vcf, "--variant",
                            filtered_indels_vcf,
                            "-o", output_vcf,
                            "-genotypeMergeOptions", "UNIQUIFY"]

    cmd_joint_filt = " ".join(cmd_joint_filt_array)

    # create an cluster option object according the
    # scheduler and set the options
    if cluster_drm is not None and drmaa_session is not None:

        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_nodes("1")

        run_local = False

        # cluster options for SNP extraction
        options_object.set_job_name(job1_snps)
        snp1_options = options_object.to_string()

        # cluster options for SNP filtering
        options_object.set_job_name(job2_snps)
        snp2_options = options_object.to_string()

        # cluster options for INDEL extraction
        options_object.set_job_name(job1_indels)
        indels1_options = options_object.to_string()

        # cluster options for INDEL filtering
        options_object.set_job_name(job2_indels)
        indels2_options = options_object.to_string()

        # cluster options for merge filtered variants
        options_object.set_job_name(job_merge_filt)
        merge_filt_options = options_object.to_string()

    else:
        snp1_options, snp2_options, indels1_options, indels2_options, merge_filt_options = "", "", "", "", ""
        run_local = True

    # varaibles to extract SNPs
    snp1_array = [cmd1_snps,
                  job1_snps,
                  snp1_options,
                  "Extracting SNPs from file "+input_vcf,
                  "ERROR: there was a problem trying to extract SNPs from file "+input_vcf]

    # variables to filter SNPs
    snp2_array = [cmd2_snps,
                  job2_snps,
                  snp2_options,
                  "Filtering SNPs from file "+input_vcf,
                  "ERROR: there was a problem trying to filter SNPs from file "+input_vcf]

    # variables to extract INDELs
    indel1_array = [cmd1_indels, job1_indels,
                    indels1_options,
                    "Extracting INDELs from file "+input_vcf,
                    "ERROR: there was a problem trying to extract INDELs from file "+input_vcf]

    # variables to filter INDELs
    indel2_array = [cmd2_indels, job2_indels, indels2_options,
                    "Filtering INDELs from file "+input_vcf,
                    "ERROR: there was a problem trying to filter INDELs from file "+input_vcf]

    # merge INDELs and SNP into one single file
    merge_filt_array = [cmd_joint_filt,
                        job_merge_filt,
                        merge_filt_options,
                        "Merging filtered SNPs and INDELs from "+input_vcf+" into one single VCF",
                        "ERROR: there was a problem trying to merge filtered variants from "+input_vcf]

    all_commands = [snp1_array, snp2_array,
                    indel1_array, indel2_array, merge_filt_array]

    # iterate over each command
    for cmd in all_commands:
        try:
            logger.log(MESSAGE, cmd[3])
            stdout_res, stderr_res = run_job(cmd_str=cmd[0],
                                             job_name=cmd[1],
                                             logger=logger,
                                             drmaa_session=drmaa_session,
                                             run_locally=run_local,
                                             job_script_directory=output_dir,
                                             job_other_options=cmd[2])
            for line in stderr_res:
                stderr_file.write(line)

        # ! remember that the exceptions are not being well throwed
        except error_drmaa_job as err:

            log_error = "\n".join(map(str,
                                      "Failed to run:",
                                      cmd[0], err, stderr_res))

            stderr_error = "\n".join(map(str, (stderr_res)))

            logger.log(MESSAGE, log_error)
            stderr_file.write(stderr_error)

            raise Exception(log_error)


# recalibrate called variants
def variant_rec(input_vcf, output_vcf, JAVA, GATK, refernece_fasta, logger, databases):
    # databases is the array with the paths to the known information databases
    pass


# annotate variants
def annotate(input_vcf, output_vcf, JAVA, SNPEFF, ref_genome, database, RAM, annot_file, logger, drmaa_session, cluster_drm):

    # add RAN option
    stderr_file = open(output_vcf.replace(".vcf", ".stderr"), "a")
    stdout_file = open(output_vcf, "a")

    output_dir = dirname(output_vcf)

    # name of the job to be sent
    job2_name = "annot_"+basename(input_vcf)

    stdout_res, stderr_res = "", ""

    # flag to check if the SNPEff db will be built
    db_is_built = False

    logger.log(MESSAGE, "Checking if "+database+" exists in SNPEff databases")
    process = Popen([JAVA+" -jar "+SNPEFF+" databases | cut -f2 | grep "+database], shell=True, stdout=PIPE)

    data = process.communicate()
    db_exists = False

    for line in data:
        if line is not None:
            line = line.rstrip("\n").strip()
            if line == database.strip():
                logger.log(MESSAGE, "There was an exact match in the SNPEff databases")
                db_exists = True
                break

    if not db_exists:
        if annot_file is not None:

            logger.log(MESSAGE, database+" database doesn't exist but annotation file was given. A new database will be built in SNPEff")
            logger.log(MESSAGE, "Setting the necessary files for the SNPEff database construction")

            # check if the db exists in the snpEff.config file
            is_present = False
            for line in open(join(dirname(SNPEFF), "snpEff.config"), "r"):
                line = line.rstrip("\n")
                if line == database+" : "+database:
                    is_present = True

            if not is_present:
                # add an entry for the db in the snpEff.config file
                call(["echo "+database+".genome : "+database+" >> "+join(dirname(SNPEFF), "snpEff.config")], shell=True)

            # create the db folder in the data folder in $SNPEFF_HOME
            snpeff_data_folder = join(dirname(SNPEFF), "data")
            if not exists(snpeff_data_folder):
                call(["mkdir", snpeff_data_folder])

            db_folder = join(snpeff_data_folder, database)
            if not exists(db_folder):
                call(["mkdir", db_folder])

            # copy the annotation file in the db folder
            if "gtf" in annot_file:
                dest_annot_file = join(db_folder, "genes.gtf")
            else:
                dest_annot_file = join(db_folder, "genes.gff")

            if not exists(dest_annot_file):
                call(["cp", annot_file, dest_annot_file])

            # copy the genome in the genomes folder in $SNPEFF_HOME/data/genome
            genome_folder = join(join(dirname(SNPEFF), "data"), "genomes")

            if not exists(genome_folder):
                call(["mkdir", genome_folder])

            dest_genome_file = join(genome_folder, database+".fa")

            if not exists(dest_genome_file):
                call(["cp", ref_genome, dest_genome_file])

            db_is_built = True

        else:
            remove(output_vcf)
            logger.log(MESSAGE, "ERROR: "+database+" database doesn't exist in SNPEff and no annotation file  was given. Please give a valid database")
            exit(1)

    if db_exists and annot_file is not None:
        logger.log(MESSAGE, "WARNING: annotation file "+basename(annot_file)+" was given but SNPEff database "+database+" exists. The database won't be built")

    # check which job scheduler was setted and write the options
    if cluster_drm is not None:

        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_nodes("1")
        run_local = False

        # cluster options for db building
        if db_is_built:
            options_object.set_job_name("build_snpeff_db")
            other_options = options_object.to_string()

        # cluster options for SNP calling
        options_object.set_job_name(job2_name)
        other2_options = options_object.to_string()

    else:
        if db_is_built:
            other_options = ""
        other2_options = ""
        run_local = True

    all_cds = []

    if db_is_built:
        if "gtf" in annot_file:
            cmd_type = "-gtf22"
        else:
            cmd_type = "-gff3"

        cmd_array = [JAVA, "-jar", SNPEFF, "build", cmd_type, "-v", database]
        cmd_clust = " ".join(cmd_array)

        cmd_vars = [cmd_clust, "build_snpeff_db", other_options,
                    "Building the SNPEff database: "+database,
                    "Error: there was an error trying to build SNPEff database"]

        all_cds.append(cmd_vars)

    cmd2_array = [JAVA, "-jar",
                  "-Xmx"+str(RAM)+"G",
                  SNPEFF, "-o",
                  "gatk", "-v",
                  database, input_vcf]

    cmd2_clust = " ".join(cmd2_array)

    cmd2_vars = [cmd2_clust, job2_name, other2_options,
                 "Annotating post filtered variants : "+input_vcf,
                 "There was an error trying to annotate variants from file "+input_vcf]

    all_cds.append(cmd2_vars)

    for cmd in all_cds:

        logger.log(MESSAGE, cmd[3])

        try:
            stdout_res, stderr_res = run_job(cmd_str=cmd[0],
                                             job_name=cmd[1],
                                             logger=logger,
                                             drmaa_session=drmaa_session,
                                             run_locally=run_local,
                                             job_script_directory=output_dir,
                                             job_other_options=cmd[2])
            for line in stderr_res:
                stderr_file.write(line)
            for line in stdout_res:
                stdout_file.write(line)

        # ! remember that the exceptions are not being well throwed
        except error_drmaa_job as err:

            log_error = "\n".join(map(str,
                                      "Failed to run:",
                                      cmd[0], err, stderr_res))
            stderr_error = "\n".join(map(str, (stderr_res)))

            logger.log(MESSAGE, log_error)
            stderr_file.write(stderr_error)
            remove(output_vcf)

            raise Exception(log_error)

    call(["mv", "snpEff_genes.txt", dirname(output_vcf)])
    call(["mv", "snpEff_summary.html", dirname(output_vcf)])