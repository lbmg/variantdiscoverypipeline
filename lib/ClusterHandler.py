class ClusterOptions():

    def __init__(self):

        self.job_name = None
        self.nodes = None
        self.ntasks = None
        self.ntasks_per_core = None
        self.ntasks_per_node = None
        self.output_file = None
        self.error_file = None
        self.partition = None
        self.task_per_node = None
        self.threads_per_core = None
        self.time_min = None
        self.mem = None
        self.mem_per_cpu = None

    def set_job_name(self, string):
        self.job_name = string

    def set_nodes(self, string):
        self.nodes = string

    def set_ntasks(self, string):
        self.ntasks = string

    def set_ntasks_per_core(self, string):
        self.ntasks_per_core = string

    def set_ntasks_per_node(self, string):
        self.ntasks_per_node = string

    def set_output_file(self, string):
        self.output_file = string

    def set_error_file(self, string):
        self.error_file = string

    def set_partition(self, string):
        self.partition = string

    def set_task_per_node(self, string):
        self.task_per_node = string

    def set_threads_per_core(self, string):
        self.threads_per_core = string

    def set_time_min(self, string):
        self.time_min = string

    def set_mem(self, string):
        self.mem = string

    def set_mem_per_cpu(self, string):
        self.mem_per_cpu = string


class SlurmOptions(ClusterOptions):
    def to_string(self):

        opts = []

        if self.job_name is not None:
            opts.append("-J "+self.job_name)

        if self.nodes is not None:
            opts.append("-N "+self.nodes)

        if self.ntasks is not None:
            opts.append("-n "+self.ntasks)

        if self.ntasks_per_core is not None:
            opts.append("--ntasks-per-core="+self.ntasks_per_core)

        if self.ntasks_per_node is not None:
            opts.append("--ntasks-per-node="+self.ntasks_per_node)

        if self.partition is not None:
            opts.append("-p "+self.partition)

        if self.task_per_node is not None:
            opts.append("--tasks-per-node="+self.task_per_node)

        if self.threads_per_core is not None:
            opts.append("--threads-per-core="+self.threads_per_core)

        if self.time_min is not None:
            opts.append("--time-min="+self.time_min)

        if self.mem is not None:
            opts.append("--mem="+self.mem)

        if self.mem_per_cpu is not None:
            opts.append("--mem-per-cpu="+self.mem_per_cpu)

        return " ".join(opts)


class CondorOptions(ClusterOptions):
    def to_string(self):
        return ""


class LSFOptions(ClusterOptions):
    def to_string(self):
        return ""


class ClusterOptionsFactory():
    # engine
    __lsf__ = 2
    __slurm__ = 1
    __condor__ = 3

    @staticmethod
    def get_options_object(drm):
        if drm == ClusterOptionsFactory.__slurm__:
            return SlurmOptions()
        elif drm == ClusterOptionsFactory.__lsf__:
            return LSFOptions()
        elif drm == ClusterOptionsFactory.__condor__:
            return CondorOptions()
        else:
            return None
