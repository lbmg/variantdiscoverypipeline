from ruffus.cmdline import MESSAGE
from ruffus.drmaa_wrapper import run_job, error_drmaa_job
from ClusterHandler import ClusterOptionsFactory
from subprocess import call
from os.path import join, exists, abspath, dirname, basename


def send_cluster_job(cmd_clust, job_name, logger, drmaa_session, run_local,
                     output_dir, other_options, stderr_file):

    stdout_res, stderr_res = "", ""
    try:
        stdout_res, stderr_res = run_job(cmd_str=cmd_clust,
                                         job_name=job_name,
                                         logger=logger,
                                         drmaa_session=drmaa_session,
                                         run_locally=run_local,
                                         job_script_directory=output_dir,
                                         job_other_options=other_options)
        for line in stderr_res:
            stderr_file.write(line)

    except error_drmaa_job as err:

        error = "\n".join(map(str, ("Failed to run:",
                                    cmd_clust,
                                    err,
                                    stderr_res)))

        logger.log(MESSAGE, error)
        stderr_file.write(error)


# build a new preprocessing project
def build_reference(project_genome, original_fasta_genome, JAVA, PICARD,
                    logger, drmaa_session, cl_drm, memory):

    # absolute path of the original FASTA of the reference genome
    fasta_genome_path = abspath(original_fasta_genome)
    project_genome_folder = dirname(project_genome)

    # genome dictionary
    project_genome_dict = project_genome.replace("fasta", "dict")

    stderr_file = open(join(project_genome_folder, "reference.stderr"), "a")
    stdout_file = open(join(project_genome_folder, "reference.stdout"), "a")

    # check if the symbolic link to the referene genome, if not then create it
    if not exists(project_genome):
        logger.log(MESSAGE, 'Creating a symbolic link for the reference genome'
                            ' in the project folder')
        call(["ln", "-s", fasta_genome_path, project_genome])

    # check if a samtool index for the link of the genome exists,
    # if not then create it
    if not exists(project_genome+".fai"):
        logger.log(MESSAGE, 'Creating a samtools index for the reference'
                            ' genome')
        call(["samtools", "faidx", project_genome],
             stderr=stderr_file,
             stdout=stdout_file)

    # check if a picard dictionary for the link of the reference genome,
    # if not then create it
    if not exists(project_genome_dict):
        logger.log(MESSAGE, 'Creating a dictionary for the refernece'
                            ' genome')
        cmd = [JAVA, "-jar",
               PICARD, "CreateSequenceDictionary",
               "R=", project_genome,
               "O=", project_genome_dict]
        call(cmd, stderr=stderr_file, stdout=stdout_file)


def define_inputs(bams_desc, logger, logger_mutex, drmaa_session,
                  cluster_drm, memory):

    cmd_clust = "samtools index "+bams_desc[0]
    job_name = "bam_indexing_"+basename(bams_desc[0]).replace(".bam", "")
    sample_name = basename(bams_desc[0]).replace(".bam", "")
    stderr_file = open(bams_desc[0].replace(".bam", ".stderr"), "w")
    output_dir = dirname(bams_desc[0])

    if not exists(bams_desc[0]):
        with logger_mutex:
            logger.log(MESSAGE, 'Creating a symbolic link inside the'
                                ' project for the input'
                                ' '+basename(bams_desc[1]))
        call(["ln", "-s", abspath(bams_desc[1]), bams_desc[0]])

    # check which job scheduler was setted and write the options
    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_job_name(job_name)
        options_object.set_nodes("1")
        options_object.set_ntasks("1")
        options_object.set_mem(str(memory))
        run_local = False

        # cluster options for SNP calling
        other_options = options_object.to_string()

    else:
        other_options = ""
        run_local = True

    with logger_mutex:
        logger.log(MESSAGE, "Indexing sample "+sample_name)

    send_cluster_job(cmd_clust, job_name, logger, drmaa_session, run_local,
                     output_dir, other_options, stderr_file)


# call variants
def gatk_calling(input_data, output_gvcf, JAVA, GATK, logger,
                 logger_mutex, nct, drmaa_session, cluster_drm, memory):

    # file where the messages of the command
    stderr_file = open(output_gvcf.replace(".vcf", ".stderr"), "w")

    output_dir = dirname(output_gvcf)
    sample_name = basename(input_data[0][0]).replace(".bam", "")

    # name of the job to be sent
    job_name = "make_calling_"+sample_name

    # set the array with the proper command to run GATK
    cmd_array = [JAVA, "-Xmx"+str(memory)+"m", "-jar",
                 GATK, "-R", input_data[1],
                 "-T", "HaplotypeCaller",
                 "-I", input_data[0][0],
                 "--emitRefConfidence", "GVCF",
                 "-o", output_gvcf,
                 "-nct", str(nct),
                 "-variant_index_type",
                 "LINEAR",
                 "-variant_index_parameter",
                 "128000"]

    cmd_clust = " ".join(cmd_array)

    # check which job scheduler was setted and write the options
    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_job_name(job_name)
        options_object.set_nodes("1")
        options_object.set_ntasks(str(nct))
        options_object.set_mem(str(memory))
        run_local = False

        # cluster options for SNP calling
        other_options = options_object.to_string()

    else:
        other_options = ""
        run_local = True

    with logger_mutex:
        logger.log(MESSAGE, "Making SNP calling for sample "+sample_name)

    send_cluster_job(cmd_clust, job_name, logger, drmaa_session, run_local,
                     output_dir, other_options, stderr_file)


# make joint genotyping
def joint_geno(input_g_vcfs, output_vcf, JAVA, GATK, genome_fasta, logger,
               drmaa_session, cluster_drm, n_theads, memory, partition):

    # file where the messages of the command
    stderr_file = open(output_vcf.replace(".vcf", ".stderr"), "w")

    output_dir = dirname(output_vcf)

    # name of the job to be sent
    job_name = "joint_genotyping"

    cmd_array = [JAVA, "-Xmx"+str(memory)+"m", "-jar",
                 GATK, "-T",
                 "GenotypeGVCFs", "-R", genome_fasta,
                 "-nt", str(n_theads)]
    inputs = []

    for i in input_g_vcfs:
        inputs += ["--variant", i]

    cmd_array += inputs + ["-o", output_vcf]

    cmd_clust = " ".join(cmd_array)

    # check which job scheduler was setted and write the options
    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_job_name(job_name)
        options_object.set_nodes("1")
        options_object.set_ntasks(str(n_theads))
        options_object.set_mem(str(memory))
        if partition is not None:
            options_object.set_partition(partition)
        run_local = False

        # cluster options for SNP calling
        other_options = options_object.to_string()

    else:
        other_options = ""
        run_local = True

    logger.log(MESSAGE, "Making joint genotyping with files:")
    for gvcf in input_g_vcfs:
        logger.log(MESSAGE, "   "+gvcf)

    send_cluster_job(cmd_clust, job_name, logger, drmaa_session, run_local,
                     output_dir, other_options, stderr_file)
