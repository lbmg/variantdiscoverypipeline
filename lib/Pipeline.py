from ruffus import cmdline
from ruffus.cmdline import MESSAGE
from ruffus import suffix
from ruffus import Pipeline
from os.path import join, exists
from os import environ, getcwd
from subprocess import call
import sys

import Preprocessing as preproc
import SNPCalling as snpcalling


###########################
# PIPELINE DEFINITION
class VariantAnalysisPipeline():

    def __init__(self, args):

        self.pipelines = []

        self.args = args

        # project folder
        self.PROJECT_FOLDER = self.args.project_name

        # number of subpipelines
        self.n_pipelines = 0

        # create the working directory
        if not exists(self.PROJECT_FOLDER):
            call(["mkdir", self.PROJECT_FOLDER])

        # define a log file
        if self.args.log_file is not None:
            self.log_file = join(self.PROJECT_FOLDER, self.args.log_file)
        else:
            self.log_file = join(self.PROJECT_FOLDER,
                                 self.PROJECT_FOLDER+".log")

        # define the logger object
        logger_title = "VariantDiscovery Pipeline::project::"
        self.logger, self.logger_mutex = cmdline.setup_logging(
            logger_title + self.PROJECT_FOLDER,
            self.log_file,
            self.args.verbose_level)

        # check if the pipeline will run in cluster or local mode
        if environ.get('DRMAA_LIBRARY_PATH') is not None\
           and self.args.cluster is not None:
                import drmaa
                # create drmaa session
                self.drmaa_session = drmaa.Session()
                self.drmaa_session.initialize()

        else:
            if self.args.cluster is not None:
                self.logger("WARNING: Cluster mode was setted but environment"
                            " variable DRMAA_LIBRARY_PATH is None. Running in"
                            " local mode")
            self.drmaa_session = None

        # path to JAVA binary file
        java_env = environ.get('JAVA_HOME')
        if java_env is not None:
            self.JAVA = join(java_env, "bin/java")
        else:
            print "JAVA_HOME environment variable not found"
            exit(1)

        # path to picard jar file
        picard_env = environ.get('PICARD_HOME')
        if picard_env is not None:
            self.PICARD = join(picard_env, "picard.jar")
        else:
            print "PICARD_HOME envirnment variable not found"
            exit(1)

        # path to GATK jar file
        gatk_env = environ.get('GATK_HOME')
        if gatk_env is not None:
            self.GATK = join(gatk_env, "GenomeAnalysisTK.jar")
        else:
            print "GATK_HOME environment variable not found"
            exit(1)

        # elif args.task == "postprocessing":
        #     # path to SNPEff jar file
        #     snpeff_env = environ.get('SNPEFF_HOME')
        #     if snpeff_env is not None:
        #         self.SNPEFF = join(environ.get('SNPEFF_HOME'), "snpEff.jar")
        #     else:
        #         print "SNPEFF_HOME envirnment variable not found"
        #         exit(1)

    def pre_processing_pipeline(self, starter_fastqs,
                                reference_genome, folders):

        pp_pipeline = Pipeline("pre_processing_pipeline")

        pp_pipeline.originate(task_func=preproc.build_reference,
                              name="Build genome indexes",
                              output=reference_genome,
                              extras=[self.args.REF_GENOME,
                                      self.JAVA,
                                      self.PICARD,
                                      self.logger,
                                      self.drmaa_session,
                                      self.args.cluster,
                                      self.args.job_mem])

        # set the pipeline
        pp_pipeline.originate(task_func=preproc.define_inputs,
                              name="Define input FASTQs",
                              output=starter_fastqs,
                              extras=[self.logger,
                                      self.logger_mutex])

        # align reads to reference genome
        pp_pipeline.transform(task_func=preproc.align_reads,
                              name="Align reads",
                              filter=suffix(".1.fastq.gz"),
                              output=".bam",
                              input=preproc.define_inputs,
                              add_inputs=tuple([preproc.build_reference]),
                              output_dir=folders[2],
                              extras=[self.logger,
                                      self.logger_mutex,
                                      self.drmaa_session,
                                      self.args.cluster,
                                      self.args.n_threads,
                                      self.args.job_mem])

        # align reads to reference genome
        pp_pipeline.transform(task_func=preproc.fix_mates,
                              name="Fix mates",
                              filter=suffix(".bam"),
                              output=".fixed_mates.bam",
                              input=preproc.align_reads,
                              output_dir=folders[3],
                              extras=[self.logger,
                                      self.logger_mutex,
                                      self.drmaa_session,
                                      self.args.cluster,
                                      self.args.job_mem])

        # align reads to reference genome
        pp_pipeline.transform(task_func=preproc.sort_alignments,
                              name="Sort alignments",
                              filter=suffix(".fixed_mates.bam"),
                              output=".sorted.bam",
                              input=preproc.fix_mates,
                              output_dir=folders[4],
                              extras=[self.logger,
                                      self.logger_mutex,
                                      self.drmaa_session,
                                      self.args.cluster,
                                      self.args.n_threads,
                                      self.args.job_mem])

        # define a pipeline task to mark duplicated
        pp_pipeline.transform(task_func=preproc.mark_duplicated,
                              name="Mark duplicates",
                              input=preproc.sort_alignments,
                              filter=suffix(".sorted.bam"),
                              output="_mdup.bam",
                              output_dir=folders[5],
                              extras=[self.JAVA,
                                      self.PICARD,
                                      self.logger,
                                      self.logger_mutex,
                                      self.drmaa_session,
                                      self.args.cluster,
                                      self.args.job_mem])

        # define a pipeline task to realign INDELs
        pp_pipeline.transform(task_func=preproc.realign_indels,
                              name="Realign indels",
                              input=preproc.mark_duplicated,
                              filter=suffix("mdup.bam"),
                              output="IN_realigned.bam",
                              output_dir=folders[6],
                              extras=[self.JAVA,
                                      self.GATK,
                                      reference_genome,
                                      self.logger,
                                      self.logger_mutex,
                                      self.drmaa_session,
                                      self.args.cluster,
                                      self.args.job_mem,
                                      self.args.n_threads])

        pp_pipeline.set_head_tasks([pp_pipeline[preproc.define_inputs]])

        if self.args.known_variants is not None:

            # define a pipeline task for mapping quality recalibration
            pp_pipeline.transform(task_func=preproc.qual_recalibration,
                                  name="Base quality recalibraton",
                                  input=preproc.mark_duplicated,
                                  filter=suffix("IN_realigned.bam"),
                                  output="recal.bam",
                                  output_dir=folders[7],
                                  extras=[self.JAVA,
                                          self.GATK,
                                          reference_genome,
                                          self.logger,
                                          self.logger_mutex,
                                          self.drmaa_session,
                                          self.args.cluster,
                                          self.args.job_mem,
                                          self.args.known_variants,
                                          self.args.n_threads])

            pp_pipeline.set_tail_tasks(
                [pp_pipeline[preproc.qual_recalibration]])

        else:
            pp_pipeline.set_tail_tasks(
                [pp_pipeline[preproc.realign_indels]])

        return pp_pipeline

    # def bostraped_calling_pipeline(self, step):

    #     bst_pipeline = Pipeline("boostraped_calling_step_"+str(step))
    #     call_folder = join(self.PROJECT_FOLDER, "calling")
    #     rq_folder = join(self.PROJECT_FOLDER, "qual_recalibration")
    #     step_pattern = "IN_realigned.bam" if step == 0 else "rq_step_"+str(step-1)+".bam"

    #     make_calling_task = bst_pipeline.transform(task_func=make_calling,
    #                                                name="Call variants step "+str(step),
    #                                                input=None,
    #                                                filter=suffix(step_pattern),
    #                                                output="gvcf_step_"+str(step)+".vcf",
    #                                                output_dir=join(call_folder,"step_"+str(step)),
    #                                                extras=[self.JAVA,
    #                                                        self.GATK,
    #                                                        self.ref_genome_project,
    #                                                        self.logger,
    #                                                        self.logger_mutex,
    #                                                        self.args.nct,
    #                                                        self.drmaa_session,
    #                                                        self.args.cluster])

    #     joint_genotyping_task = bst_pipeline.merge(task_func=joint_genotyping,
    #                                                name="Joint genotyping step "+str(step),
    #                                                input=make_calling,
    #                                                output=join(call_folder, "variants_step_"+str(step)+".vcf"),
    #                                                extras=[self.JAVA, self.GATK, 
    #                                                        self.ref_genome_project,
    #                                                        self.logger,
    #                                                        self.drmaa_session,
    #                                                        self.args.cluster])

    #     if step < (self.args.n_bst-1):
    #         recalibration_task = bst_pipeline.transform(task_func=recalibrate_qualities,
    #                                                     name="Recalibrate qualities step "+str(step),
    #                                                     input=None,
    #                                                     filter=suffix(step_pattern),
    #                                                     add_inputs=(joint_genotyping,),
    #                                                     output="rq_step_"+str(step)+".bam",
    #                                                     output_dir=join(rq_folder, "step_"+str(step)),
    #                                                     extras=[self.JAVA,
    #                                                             self.GATK,
    #                                                             self.ref_genome_project,
    #                                                             self.args.nct,
    #                                                             self.logger,
    #                                                             self.logger_mutex,
    #                                                             self.drmaa_session,
    #                                                              self.args.cluster])

    #         bst_pipeline.set_head_tasks([bst_pipeline[make_calling],
    #                                      bst_pipeline[recalibrate_qualities]])
    #         bst_pipeline.set_tail_tasks([bst_pipeline[recalibrate_qualities]])

    #     else:
    #         bst_pipeline.set_head_tasks([bst_pipeline[make_calling]])
    #         bst_pipeline.set_tail_tasks([bst_pipeline[joint_genotyping]])

    #     return bst_pipeline

    def gatk_calling_pipeline(self, bams_names, output_folder,
                              reference_genome):

        gatk_pipeline = Pipeline("GATK_calling_pipeline")

        gatk_pipeline.originate(
            task_func=snpcalling.build_reference,
            name="Build genome indexes",
            output=reference_genome,
            extras=[self.args.REF_GENOME,
                    self.JAVA,
                    self.PICARD,
                    self.logger,
                    self.drmaa_session,
                    self.args.cluster,
                    self.args.job_mem])

        # set the pipeline
        gatk_pipeline.originate(
            task_func=snpcalling.define_inputs,
            name="Define input BAMs",
            output=bams_names,
            extras=[self.logger,
                    self.logger_mutex,
                    self.drmaa_session,
                    self.args.cluster,
                    self.args.job_mem])

        gatk_pipeline.transform(
            task_func=snpcalling.gatk_calling,
            name="Call variants",
            input=snpcalling.define_inputs,
            filter=suffix(".bam"),
            output=".snps.indels.g.vcf",
            output_dir=output_folder,
            add_inputs=tuple([snpcalling.build_reference]),
            extras=[self.JAVA,
                    self.GATK,
                    self.logger,
                    self.logger_mutex,
                    self.args.n_threads,
                    self.drmaa_session,
                    self.args.cluster,
                    self.args.job_mem])

        gatk_pipeline.merge(
            task_func=snpcalling.joint_geno,
            name="Joint genotyping",
            input=snpcalling.gatk_calling,
            output=join(output_folder, "raw_vatiants.vcf"),
            extras=[self.JAVA,
                    self.GATK,
                    reference_genome,
                    self.logger,
                    self.drmaa_session,
                    self.args.cluster,
                    self.args.n_joint_threads,
                    self.args.joint_mem,
                    self.args.part])

        gatk_pipeline.set_head_tasks([gatk_pipeline[snpcalling.gatk_calling]])
        gatk_pipeline.set_tail_tasks([gatk_pipeline[snpcalling.joint_geno]])

        return gatk_pipeline

    # def post_processing_pipeline(self):

    #     pop_pipeline = Pipeline("post_processing_pipeline")
    #     report_folder = join(self.PROJECT_FOLDER, "report")

    #     # ! here I have to add the hard filtering options
    #     if self.args.pp_task == 0:
    #         name = "Hard filter variants"
    #         step_funct = hard_filtering
    #         extras = [self.JAVA, self.GATK,
    #                   self.ref_genome_project, self.args.hard_filt_options_snps,
    #                   self.args.hard_filt_options_indels, self.logger,
    #                   self.drmaa_session, self.args.cluster]

    #     else:
    #         # ! here i have to add the databases
    #         name = "Recalibrate variant qualities"
    #         step_funct = variant_rec
    #         extras = [self.JAVA, self.GATK,
    #                   self.ref_genome_project,
    #                   self.args.recalibration_dbs,
    #                   self.logger,
    #                   self.drmaa_session,
    #                   self.args.cluster]

    #     # define a pipeline task to filter called variants
    #     filter_snps_task = pop_pipeline.transform(task_func=step_funct,
    #                                               name=name,
    #                                               input=None,
    #                                               filter=suffix(".vcf"),
    #                                               output="_filtered.vcf",
    #                                               output_dir=join(self.PROJECT_FOLDER+"/post_processing"),
    #                                               extras=extras)

    #     # define a pipeline task to annotate called variants
    #     ann_snps_task = pop_pipeline.transform(task_func=annotate,
    #                                            name="Annotate variants",
    #                                            input=step_funct,
    #                                            filter=suffix("_filtered.vcf"),
    #                                            output="_annotated.vcf",
    #                                            output_dir=join(self.PROJECT_FOLDER+"/annotation"),
    #                                            extras=[self.JAVA,
    #                                                    self.SNPEFF,
    #                                                    self.ref_genome_project,
    #                                                    self.args.snpeff_db,
    #                                                    self.args.snpeff_ram,
    #                                                    self.args.snpeff_annot,
    #                                                    self.logger,
    #                                                    self.drmaa_session,
    #                                                    self.args.cluster])

    #     pop_pipeline.set_head_tasks([pop_pipeline[step_funct]])
    #     pop_pipeline.set_tail_tasks([pop_pipeline[annotate]])

    #     return pop_pipeline

    def setup(self):

        # original inputs

        if self.args.task == "preprocessing":

            self.n_pipelines += 1

            # folders
            folders = [join(self.PROJECT_FOLDER, 'reference'),
                       join(self.PROJECT_FOLDER, 'fastq_files'),
                       join(self.PROJECT_FOLDER, 'alignments'),
                       join(self.PROJECT_FOLDER, 'fix_mates'),
                       join(self.PROJECT_FOLDER, 'sort_alignments'),
                       join(self.PROJECT_FOLDER, 'mark_duplicated'),
                       join(self.PROJECT_FOLDER, 'INDEL_realign')]

            if self.args.known_variants is not None:
                folders.append(join(self.PROJET_FOLDER, 'qual_recalibration'))

            # create the folders
            for folder in folders:
                if not exists(folder):
                    call(["mkdir", folder])

            # print workflow configuration
            preproc.print_conf(self.args, self.logger,
                               self.PICARD, self.drmaa_session)

            starter_fastqs = [[join(self.PROJECT_FOLDER,
                               "fastq_files", sample[0]+".1.fastq.gz"),
                               join(self.PROJECT_FOLDER,
                               "fastq_files", sample[0]+".2.fastq.gz"),
                               sample[1], sample[2]]
                              for sample in self.args.INPUT]

            reference_genome = join(folders[0], "reference.fasta")

            self.pipelines.append(self.pre_processing_pipeline(
                starter_fastqs,
                reference_genome,
                folders))

        elif self.args.task == "gatk_calling":

            self.n_pipelines += 1

            snps_folder = join(self.PROJECT_FOLDER, "gatk_calling")
            bams_folder = join(self.PROJECT_FOLDER, "input_bams")
            reference_folder = join(self.PROJECT_FOLDER, "reference")

            if not exists(snps_folder):
                call(["mkdir", snps_folder])

            if not exists(reference_folder):
                call(["mkdir", reference_folder])

            if not exists(bams_folder):
                call(["mkdir", bams_folder])

            reference_genome = join(reference_folder, "reference.fasta")

            bams_desc = [[join(bams_folder, sample[0]+".bam"),
                          join(bams_folder, sample[1])]
                         for sample in self.args.INPUT]

            # ! define a function to print configuration
            self.pipelines.append(self.gatk_calling_pipeline(
                bams_desc,
                snps_folder,
                reference_genome))


        # p_number += 1

        # if self.args.known_variants == None:
        #     for step in range(0, self.args.n_bst):
        #         self.pipelines[p_number] = self.bostraped_calling_pipeline(step)
        #         self.pipelines[p_number].set_input(input=[self.pipelines[p_number-1]])
        #         p_number += 1
        # else:
        #     self.pipelines[p_number] = self.regular_calling_pipeline()
        #     self.pipelines[p_number].set_input(input=[[self.pipelines[p_number-1]]])
        #     p_number += 1

        # self.pipelines[p_number] = self.post_processing_pipeline()

        # self.pipelines[p_number].set_input(input=[self.pipelines[p_number-1]])

    def start(self):

        # obtain the current working directory
        project_dir = join(getcwd(), self.args.project_name)

        # define a file for the checksums
        check_sum_file = "."+self.args.project_name+".sqlite"
        hist_file = join(project_dir, check_sum_file)

        if self.args.just_print:
            self.pipelines[self.n_pipelines-1].printout(sys.stdout,
                                                        verbose=5,
                                                        history_file=hist_file)
        if self.args.print_graph:
            self.pipelines[self.n_pipelines-1].printout_graph(
                join(self.PROJECT_FOLDER,
                     self.PROJECT_FOLDER+"_flowchart.svg"),
                "svg",
                history_file=hist_file,
                user_colour_scheme={"colour_scheme_index": 6})

        if not self.args.print_graph and not self.args.just_print:
            self.pipelines[self.n_pipelines-1].run(
                logger=self.logger,
                multiprocess=self.args.n_jobs,
                history_file=hist_file,
                touch_files_only=self.args.touch)

            self.logger.log(MESSAGE, "Pipeline finished")
