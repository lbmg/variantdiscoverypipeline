from subprocess import call, Popen, PIPE
from os.path import join, exists, abspath, dirname, basename
from sys import exit
from os import listdir, remove
from ruffus.cmdline import MESSAGE
from ruffus.drmaa_wrapper import run_job, error_drmaa_job
from ClusterHandler import ClusterOptionsFactory


##################
# FUNCTIONS CALLED DURING THE PIPELINE EXCECUTION
def print_conf(args, logger, PICARD, GATK, SNPEFF, cluster_mode):
    logger.log(MESSAGE, "INITIAL CONFIGURATION:")
    logger.log(MESSAGE, "Path to PICARD		: "+PICARD)
    logger.log(MESSAGE, "Path to GATK		: "+GATK)
    logger.log(MESSAGE, "Path to SNPEFF		: "+SNPEFF)
    logger.log(MESSAGE, "Path to reference genome	: "+abspath(args.REF_GENOME))
    logger.log(MESSAGE, "Input BAM files		: ")
    for input in [args.BAM_FOLDER+"/"+f for f in listdir(args.BAM_FOLDER)]:
        logger.log(MESSAGE, "	"+abspath(input))
    logger.log(MESSAGE, "Project folder		: "+abspath(args.project_name))
    logger.log(MESSAGE, "Cluster mode		: "+str(cluster_mode))
    logger.log(MESSAGE, "Number of jobs per task	: "+str(args.n_jobs))
    logger.log(MESSAGE, "Known variants file		: "+str(args.known_variants))

    if args.known_variants is None:
        approach = "Boostraped calling"
    else:
        approach = "Regular calling"

    logger.log(MESSAGE, "Variant calling approach	: "+approach)
    logger.log(MESSAGE, "Variant calling algorithm	: HaplotypeCaller")
    logger.log(MESSAGE, "CPU threads for calling	: "+str(args.nct))

    if args.pp_task == 0:
        logger.log(MESSAGE, "Post filtering task		: hard filtering")
        logger.log(MESSAGE, "SNPs filtering:")
        logger.log(MESSAGE, "	QualByDepth		: "+str(args.filt_qd[0]))
        logger.log(MESSAGE, "	FisherStrand    	: "+str(args.filt_fs[0]))
        logger.log(MESSAGE, "	RMSMappingQuality	: "+str(args.filt_mq))
        logger.log(MESSAGE, "	MQRankSum		: "+str(args.filt_mqrs))
        logger.log(MESSAGE, "	ReadPosRankSum		: "+str(args.filt_rprs[0]))
        logger.log(MESSAGE, "INDELs filtering:")
        logger.log(MESSAGE, "	QualByDepth		: "+str(args.filt_qd[1]))
        logger.log(MESSAGE, "	FisherStrand    	: "+str(args.filt_fs[1]))
        logger.log(MESSAGE, "	ReadPosRankSum		: "+str(args.filt_rprs[1]))
    else:

        logger.log(MESSAGE, "Post filtering task		: variant recalibration")

        if args.vr_hapmap is not None:
            logger.log(MESSAGE, "Hapmap db VCF		: "+args.vr_hapmap[0])
            logger.log(MESSAGE, "Prior prob Hapmap		: "+args.vr_hapmap[1])

        if args.vr_omni is not None:
            logger.log(MESSAGE, "Omni db VCF			: "+args.vr_omni[0])
            logger.log(MESSAGE, "Prior prob Omni		: "+args.vr_omni[1])

        if args.vr_otg is not None:
            logger.log(MESSAGE, "1000G db VCF		: "+args.vr_otg[0])
            logger.log(MESSAGE, "Prior prob 1000G		: "+args.vr_otg[1])

        if args.vr_dbsnp is not None:
            logger.log(MESSAGE, "dbSNP db VCF		: "+args.vr_dbsnp[0])
            logger.log(MESSAGE, "Prior prob dbSNP		: "+args.vr_dbsnp[1])

        if args.vr_mills is not None:
            logger.log(MESSAGE, "Mills db VCF		: "+args.vr_mills[0])
            logger.log(MESSAGE, "Mills prob Mills		: "+args.vr_mills[1])

    logger.log(MESSAGE, "SNPEff database		: "+args.snpeff_db)
    logger.log(MESSAGE, "SNPEff amount of RAM	: "+str(args.snpeff_ram)+" GB")


# set reference
def build_project(fasta_genome, project_folder, n_steps, JAVA, PICARD, logger):
    # folders
    folders = [join(project_folder, 'reference'),
               join(project_folder, 'alignments'),
               join(project_folder, 'sorting'),
               join(project_folder, 'mark_duplicated'),
               join(project_folder, 'INDEL_realign'),
               join(project_folder, 'calling'),
               join(project_folder, 'qual_recalibration'),
               join(project_folder, 'post_processing'),
               join(project_folder, 'annotation'),
               join(project_folder, 'report')]

    # create the folders
    for folder in folders:
        if not exists(folder):
            call(["mkdir", folder])

    # create folders for boostraping in the calling folder and the
    # recalibration folder
    if n_steps != 0:
        for step in range(0, n_steps):
            c_folder = join(folders[5], "step_"+str(step))
            r_folder = join(folders[6], "step_"+str(step))
            if not exists(c_folder):
                call(["mkdir", c_folder])
            if not exists(r_folder) and step < (n_steps-1):
                call(["mkdir", r_folder])

    # absolute path of the original FASTA of the reference genome
    fasta_genome_path = abspath(fasta_genome)

    # path of the symbolic link of the original FASTA reference genome
    path_genome_slink = join(folders[0], "reference_genome.fasta")

    # ! arreglar esto
    path_genome_slink_dict = path_genome_slink.replace("fasta", "dict")

    stderr_file = open(join(folders[0], "reference.stderr"), "a")
    stdout_file = open(join(folders[0], "reference.stdout"), "a")

    # check if the symbolic link to the referene genome, if not then create it
    if not exists(path_genome_slink):
        logger.log(MESSAGE, "Creating a symbolic link for the reference genome in the project folder")
        call(["ln", "-s", fasta_genome_path, path_genome_slink])

    # check if a samtool index for the link of the genome exists,
    # if not then create it
    if not exists(path_genome_slink+".fai"):
        logger.log(MESSAGE, "Creating a samtools index for the reference genome")
        call(["samtools", "faidx", path_genome_slink],
             stderr=stderr_file,
             stdout=stdout_file)

    # check if a picard dictionary for the link of the reference genome,
    # if not then create it
    if not exists(path_genome_slink_dict):
        logger.log(MESSAGE, "Creating a dictionary for the refernece genome")
        cmd = [JAVA, "-jar",
               PICARD, "CreateSequenceDictionary",
               "R=", path_genome_slink,
               "O=", path_genome_slink_dict]
        call(cmd, stderr=stderr_file, stdout=stdout_file)


# set input
def define_inputs(input_file, orig_folder, logger, logger_mutex):
    if not exists(input_file):
        input_name = input_file.split("/")[-1]
        orig_file = join(abspath(orig_folder), input_name)
        with logger_mutex:
            logger.log(MESSAGE, "Creating a symbolic link inside the project for the input "+orig_file)
        call(["ln", "-s", orig_file, input_file])


# create statistics about each step of the pipeline
def create_stats():
    pass


# build a report of the pipeline
def build_report():
    pass
