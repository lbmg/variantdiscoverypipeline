from ruffus.cmdline import MESSAGE
from ruffus.drmaa_wrapper import run_job, error_drmaa_job
from ClusterHandler import ClusterOptionsFactory
from subprocess import call
from os.path import join, exists, abspath, dirname, basename


def send_cluster_job(cmd_clust, job_name, logger, drmaa_session, run_local,
                     output_dir, other_options, stderr_file):

    stdout_res, stderr_res = "", ""
    try:
        stdout_res, stderr_res = run_job(cmd_str=cmd_clust,
                                         job_name=job_name,
                                         logger=logger,
                                         drmaa_session=drmaa_session,
                                         run_locally=run_local,
                                         job_script_directory=output_dir,
                                         job_other_options=other_options)
        for line in stderr_res:
            stderr_file.write(line)

    except error_drmaa_job as err:

        error = "\n".join(map(str, ("Failed to run:",
                                    cmd_clust,
                                    err,
                                    stderr_res)))

        logger.log(MESSAGE, error)
        stderr_file.write(error)

        # raise Exception(error)


# set input
def define_inputs(new_fastq_files, logger, logger_mutex):
    if not exists(new_fastq_files[0]):
        with logger_mutex:
            logger.log(MESSAGE, 'Creating a symbolic link inside the'
                                ' project for the input'
                                ' '+basename(new_fastq_files[2]))
        call(["ln", "-s", abspath(new_fastq_files[2]), new_fastq_files[0]])

    if not exists(new_fastq_files[1]):
        with logger_mutex:
            logger.log(MESSAGE, 'Creating a symbolic link inside the'
                                ' project for the input'
                                ' '+basename(new_fastq_files[3]))
        call(["ln", "-s", abspath(new_fastq_files[3]), new_fastq_files[1]])


def print_conf(args, logger, PICARD, drma_sessions):

    if drma_sessions is not None:
        cluster_mode = True
    else:
        cluster_mode = False

    logger.log(MESSAGE, "PREPROCESSING INITIAL CONFIGURATION:")
    logger.log(MESSAGE, "Path to PICARD: "+PICARD)
    logger.log(MESSAGE, "Path to reference genome: "+abspath(args.REF_GENOME))
    logger.log(MESSAGE, "Input FASTQ files: ")
    for sample in args.INPUT:
        logger.log(MESSAGE, "   "+sample[0]+":")
        logger.log(MESSAGE, "   "+sample[1])
        logger.log(MESSAGE, "   "+sample[2]+"\n")
    logger.log(MESSAGE, "Project folder: "+abspath(args.project_name))
    logger.log(MESSAGE, "Cluster mode: "+str(cluster_mode))
    if cluster_mode:
        logger.log(MESSAGE, "Jobs simultaneously executed: "+str(args.n_jobs))
        logger.log(MESSAGE, "Maximum amount of memory per job: "+str(args.job_mem)+" MB")
    logger.log(MESSAGE, "Known variants file: "+str(args.known_variants))

    if args.known_variants is not None:
        logger.log(MESSAGE, "Known variants file: "+str(args.known_variants))
        bqr = True
    else:
        bqr = False
        logger.log(MESSAGE, "Base quality recalibration: "+str(bqr))


# build a new preprocessing project
def build_reference(project_genome, original_fasta_genome, JAVA, PICARD,
                    logger, drmaa_session, cl_drm, memory):

    # absolute path of the original FASTA of the reference genome
    fasta_genome_path = abspath(original_fasta_genome)
    project_genome_folder = dirname(project_genome)

    # ! arreglar esto
    project_genome_dict = project_genome.replace("fasta", "dict")

    stderr_file = open(join(project_genome_folder, "reference.stderr"), "a")
    stdout_file = open(join(project_genome_folder, "reference.stdout"), "a")

    # check if the symbolic link to the referene genome, if not then create it
    if not exists(project_genome):
        logger.log(MESSAGE, 'Creating a symbolic link for the reference genome'
                            ' in the project folder')
        call(["ln", "-s", fasta_genome_path, project_genome])

    # check if a samtool index for the link of the genome exists,
    # if not then create it
    if not exists(project_genome+".fai"):
        logger.log(MESSAGE, 'Creating a samtools index for the reference'
                            ' genome')
        call(["samtools", "faidx", project_genome],
             stderr=stderr_file,
             stdout=stdout_file)

    # check if a picard dictionary for the link of the reference genome,
    # if not then create it
    if not exists(project_genome_dict):
        logger.log(MESSAGE, 'Creating a dictionary for the refernece'
                            ' genome')
        cmd = [JAVA, "-jar",
               PICARD, "CreateSequenceDictionary",
               "R=", project_genome,
               "O=", project_genome_dict]
        call(cmd, stderr=stderr_file, stdout=stdout_file)

    if not exists(project_genome+".bwt"):
        logger.log(MESSAGE, 'Creating a bwa index for the refernece genome')
        cmd = ["bwa", "index", project_genome]

        cmd_clust = " ".join(cmd)
        job_name = "index_ref"

        if cl_drm is not None and drmaa_session is not None:
            options_object = ClusterOptionsFactory.get_options_object(cl_drm)
            options_object.set_job_name(job_name)
            options_object.set_nodes("1")
            options_object.set_ntasks("1")
            options_object.set_mem(str(memory))
            other_options = options_object.to_string()
            run_local = False

        else:
            other_options = ""
            run_local = True

        send_cluster_job(cmd_clust, job_name, logger,
                         drmaa_session, run_local, project_genome_folder,
                         other_options, stderr_file)


def align_reads(input_data, output_bam, logger, logger_mutex, drmaa_session,
                cluster_drm, n_threads, memory):

    output_dir = dirname(output_bam)
    stderr_file = open(output_bam.replace(".bam", ".stderr"), "w")
    output_sam = output_bam.replace(".bam", ".sam")

    sample_name = basename(input_data[0][0]).replace(".1.fastq.gz", "")

    job_name_1 = "align_reads_"+sample_name
    job_name_2 = "sam_to_bam_"+sample_name

    cmd_clust = "bwa mem -t "+str(n_threads)+" -R "\
                "\"@RG\\tID:"+sample_name+"\\tSM:"+sample_name+"_SM\""\
                " "+input_data[1]+" "+input_data[0][0]+" "+input_data[0][1]+""\
                " > "+output_sam

    with logger_mutex:
        logger.log(MESSAGE, "Aligning reads from sample "+sample_name)

    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_job_name(job_name_1)
        options_object.set_nodes("1")
        options_object.set_ntasks(str(n_threads))
        options_object.set_mem(str(memory))
        other_options = options_object.to_string()
        run_local = False
    else:
        run_local = True

    send_cluster_job(cmd_clust, job_name_1, logger,
                     drmaa_session, run_local,
                     output_dir, other_options, stderr_file)

    with logger_mutex:
        logger.log(MESSAGE, 'Converting sample '+sample_name+' from SAM '
                            'to BAM')

    options_object.set_job_name(job_name_2)
    cmd_clust_2 = 'samtools view -bS '+output_sam+' -o'+output_bam
    other_options = options_object.to_string()

    send_cluster_job(cmd_clust_2, job_name_2, logger,
                     drmaa_session, run_local,
                     output_dir, other_options, stderr_file)

    call(["rm", output_sam])


def fix_mates(input_bam, output_bam, logger, logger_mutex, drmaa_session,
              cluster_drm, memory):

    output_dir = dirname(input_bam)
    stderr_file = open(output_bam.replace(".bam", ".stderr"), "w")

    sample_name = basename(input_bam).replace(".bam", "")

    job_name = "fix_mates_"+sample_name

    # map to genome, fix mates and sort
    cmd = ["samtools", "fixmate", "-O", "bam", input_bam, output_bam]

    cmd_clust = " ".join(cmd)

    with logger_mutex:
        logger.log(MESSAGE, "Fixing mates from sample "+sample_name)

    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_job_name(job_name)
        options_object.set_nodes("1")
        options_object.set_mem(str(memory))
        other_options = options_object.to_string()
        run_local = False
    else:
        run_local = True

    send_cluster_job(cmd_clust, job_name, logger,
                     drmaa_session, run_local,
                     output_dir, other_options, stderr_file)


def sort_alignments(input_bam, output_bam, logger, logger_mutex, drmaa_session,
                    cluster_drm, n_threads, memory):

    output_dir = dirname(input_bam)
    stderr_file = open(output_bam.replace(".bam", ".stderr"), "w")

    sample_name = basename(input_bam).replace(".fixed_mates.bam", "")

    job_name = "sort_aligns_"+sample_name

    # map to genome, fix mates and sort
    cmd = ["samtools", "sort", "-@"+str(n_threads), "-T",
           sample_name+".tmp", "-O", "bam", "-o", output_bam, input_bam]

    cmd_clust = " ".join(cmd)

    with logger_mutex:
        logger.log(MESSAGE, "Sorting alingments sample "+sample_name)

    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_job_name(job_name)
        options_object.set_nodes("1")
        options_object.set_mem(str(memory))
        options_object.set_ntasks(str(n_threads))
        other_options = options_object.to_string()
        run_local = False
    else:
        run_local = True

    send_cluster_job(cmd_clust, job_name, logger,
                     drmaa_session, run_local,
                     output_dir, other_options, stderr_file)


# mark duplicated entries
def mark_duplicated(input_bam, output_bam, JAVA, PICARD, logger, logger_mutex,
                    drmaa_session, cluster_drm, memory):

    # output file
    stderr_file = open(output_bam.replace(".bam", ".stderr"), "w")
    # input name
    input_name = basename(input_bam).replace(".sorted.bam", "")

    # names of the jobs to run
    job_name = "Mark_dup_"+basename(input_bam)
    job2_name = "Samtool_index_"+basename(output_bam)

    # working directory where the files will be created
    output_dir = dirname(output_bam)

    # command to run Picard
    cmd_array = [JAVA, "-jar",
                 PICARD, "MarkDuplicates",
                 "MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=", "1000",
                 "INPUT=", input_bam,
                 "OUTPUT=", output_bam,
                 "METRICS_FILE=", output_bam.replace(".bam", "_metrics.txt")]

    # comand to run samtools
    cmd2_array = ["samtools", "index", output_bam]

    cmd_clust = " ".join(cmd_array)
    cmd2_clust = " ".join(cmd2_array)

    # check which job scheduler will be used and create the
    # options to run the job
    if cluster_drm is not None and drmaa_session is not None:
        # create cluster options object
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_nodes("1")
        options_object.set_ntasks("1")
        options_object.set_mem(str(memory))

        # mark duplicates options
        options_object.set_job_name(job_name)
        other_options = options_object.to_string()

        # samtools index options
        options_object.set_job_name(job2_name)
        other2_options = options_object.to_string()

        run_local = False

    # if not job scheduler is setted, then run locally
    else:
        other_options, other2_options = "", ""
        run_local = True

    mark_dup_vars = [cmd_clust, job_name, other_options,
                     'Marking duplicated reads in sample '+input_name,
                     'ERROR: there was a problem trying to mark'
                     ' duplicates in sample  '+input_bam]

    index_bam_vars = [cmd2_clust, job2_name, other2_options,
                      'Indexing bam file from sample '+input_name,
                      'ERROR: there was a problem trying to '
                      ' indexing sample  '+input_bam]

    all_cmds = [mark_dup_vars, index_bam_vars]

    for cmd in all_cmds:
        with logger_mutex:
            logger.log(MESSAGE, cmd[3])
        send_cluster_job(cmd[0], cmd[1], logger,
                         drmaa_session, run_local,
                         output_dir, cmd[2], stderr_file)


# realign INDELs
def realign_indels(input_bam, output_bam, JAVA, GATK, genome_fasta, logger,
                   logger_mutex, drmaa_session, cluster_drm,
                   memory, n_threads):

    # add known INDELs
    stderr_file = open(output_bam.replace(".bam", ".stderr"), "a")

    output_dir = dirname(output_bam)

    input_name = basename(input_bam).replace("_mdup.bam", "")

    job1_name = "detec_in_"+input_name
    job2_name = "real_in_"+input_name

    # command to get conflictive regions
    cmd1_array = [JAVA, "-jar",
                  GATK, "-T", "RealignerTargetCreator",
                  "-R", genome_fasta,
                  "-I", input_bam,
                  "-nt", str(n_threads),
                  "-o", output_bam.replace(".bam", ".intervals")]

    # command to realign reads in conflictive regions
    cmd2_array = [JAVA, "-jar",
                  GATK, "-T", "IndelRealigner",
                  "-R", genome_fasta,
                  "-I", input_bam,
                  "-targetIntervals", output_bam.replace(".bam", ".intervals"),
                  "-o", output_bam]

    cmd1_clust = " ".join(cmd1_array)
    cmd2_clust = " ".join(cmd2_array)

    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_nodes("1")
        options_object.set_ntasks(str(n_threads))
        options_object.set_mem(str(memory))
        run_local = False

        # cluster options for target creator
        options_object.set_job_name(job1_name)
        other1_options = options_object.to_string()

        # cluster options for INDEL realignment
        options_object.set_job_name(job2_name)
        options_object.set_ntasks("1")
        other2_options = options_object.to_string()

    else:
        other1_options, other2_options = "", ""
        run_local = True

    pred_reg_vars = [cmd1_clust, job1_name, other1_options,
                     'Predicting regions to realign in sample '+input_name,
                     'ERROR: there was a problem trying to predict target '
                     'regions for realignment in sample '+input_bam]

    real_indels_vars = [cmd2_clust, job2_name, other2_options,
                        'Realigning INDELs in sample '+input_name,
                        'ERROR: there was a problem trying to realign '
                        'INDELs in sample  '+input_bam]

    all_cmds = [pred_reg_vars, real_indels_vars]

    for cmd in all_cmds:

        with logger_mutex:
            logger.log(MESSAGE, cmd[3])

        send_cluster_job(cmd[0], cmd[1], logger,
                         drmaa_session, run_local,
                         output_dir, cmd[2], stderr_file)


# realign INDELs
def qual_recalibration(input_bam, output_bam, JAVA, GATK, genome_fasta, logger,
                       logger_mutex, drmaa_session, cluster_drm, memory,
                       n_threads, known_variants):

    # add known INDELs
    stderr_file = open(output_bam.replace(".bam", ".stderr"), "a")
    output_dir = dirname(output_bam)
    output_recal = basename(output_bam).replace(".bam", ".recal_data.table")
    input_name = basename(input_bam)

    job1_name = "qual_recal_"+input_name
    job2_name = "print_recal_"+input_name

    # recalibrate qualities
    cmd1_array = [JAVA, "-jar",
                  GATK, "-T", "BaseRecalibrator",
                  "-R", genome_fasta,
                  "-I", input_bam,
                  "-nct", str(n_threads),
                  "-knownSites", known_variants,
                  "-o", output_recal]

    # print recalibration
    cmd2_array = [JAVA, "-jar",
                  GATK, "-T", "PrintReads",
                  "-R", genome_fasta,
                  "-I", input_bam,
                  "-nct", str(n_threads),
                  "--BQSR", output_recal,
                  "-o", output_bam]

    cmd1_clust = " ".join(cmd1_array)
    cmd2_clust = " ".join(cmd2_array)

    if cluster_drm is not None and drmaa_session is not None:
        options_object = ClusterOptionsFactory.get_options_object(cluster_drm)
        options_object.set_nodes("1")
        options_object.set_ntasks(str(n_threads))
        options_object.set_mem(str(memory))
        run_local = False

        # cluster options for target creator
        options_object.set_job_name(job1_name)
        other1_options = options_object.to_string()

        # cluster options for INDEL realignment
        options_object.set_job_name(job2_name)
        other2_options = options_object.to_string()

    else:
        other1_options, other2_options = "", ""
        run_local = True

    pred_reg_vars = [cmd1_clust, job1_name, other1_options,
                     'Recalibrating qualities in sample '+input_name,
                     'ERROR: there was a problem trying to recalibrate '
                     'qualities in sample '+input_bam]

    real_indels_vars = [cmd2_clust, job2_name, other2_options,
                        'Printing recalibrated reads of sample '+input_name,
                        'ERROR: there was a problem trying to print '
                        'recalibrated qualities in sample  '+input_bam]

    all_cmds = [pred_reg_vars, real_indels_vars]

    for cmd in all_cmds:

        with logger_mutex:
            logger.log(MESSAGE, cmd[3])

        send_cluster_job(cmd[0], cmd[1], logger,
                         drmaa_session, run_local,
                         output_dir, cmd[2], stderr_file)
