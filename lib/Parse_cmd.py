import argparse
from sys import argv
from os.path import exists


####################
# ARGUMENT PARSER
def is_valid_file(parser, arg):

    if not exists(arg):
        parser.error("The file %s does not exist" % arg)
    else:
        return arg


def is_gff_or_gtf(parser, arg):

    if not exists(arg):
        parser.error("The file %s does not exist" % arg)
    else:
        if "gff" not in arg and "gtf" not in arg:
            parser.error("The file %s must have gtf or gff extension" % arg)
        else:
            return arg


class VDPArgumentParser(argparse.ArgumentParser):
    def parse_args(self):
        args = super(VDPArgumentParser, self).parse_args()

        # # set the options for variant post-processing
        # if args.pp_task == 0:
        #     # hard filtering

        #     args.filters_snps = ['QD < '+str(args.filt_qd[0]),
        #                          'FS > '+str(args.filt_fs[0]),
        #                          'MQ < '+str(args.filt_mq),
        #                          'MQRankSum < '+str(args.filt_mqrs),
        #                          'ReadPosRankSum < '+str(args.filt_rprs[0])]

        #     args.filters_indels = ['QD < '+str(args.filt_qd[1]),
        #                            'FS > '+str(args.filt_fs[1]),
        #                            'ReadPosRankSum < '+str(args.filt_rprs[1])]

        #     args.hard_filt_options_snps = " || ".join(args.filters_snps)
        #     args.hard_filt_options_indels = " || ".join(args.filters_indels)

        # else:
        #     # variant recalibration
        #     input_dbs = 0
        #     tmp = [args.vr_hapmap,
        #            args.vr_omni,
        #            args.vr_otg,
        #            args.vr_dbsnp,
        #            args.vr_mills]

        #     for db in tmp:
        #         if db is not None:
        #             input_dbs +=1

        #     if input_dbs == 0:
        #         print "VariantDiscovery: error: with argument -ps/--pprocess-task = 1, at least one database is needed for variant recalibration"
        #         exit(1)
        #     else:
        #         args.recalibration_dbs = tmp

        # # check database for variant annotation
        # if args.snpeff_db is None:
        #     args.snpeff_db = "".join(basename(args.REF_GENOME).split(".")[:-1])

        return args


class BamFilesArgumentAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):

        if not exists(values):
            raise argparse.ArgumentError(self, 'file '+values+' doesn\'t exist')

        fd = open(values, "r")
        bam_files = []
        for line in fd:
            fields = line.rstrip("\n").split("\t")
            if len(fields) == 2:
                if not exists(fields[1]):
                    raise argparse.ArgumentError(self, 'file '+fields[1]+' doesn\'t exist')
                else:
                    bam_files.append(fields)
            else:
                raise argparse.ArgumentError(self, 'file '+values+' must be tab separated')

        if len(bam_files) == 0:
            raise argparse.ArgumentError(self, 'file '+values+' has no samples')

        setattr(namespace, self.dest, bam_files)


class FastqFilesArgumentAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):

        if not exists(values):
            raise argparse.ArgumentError(self, 'file '+values+' doesn\'t exist')

        fd = open(values, "r")
        fastq_files = []
        for line in fd:
            fields = line.rstrip("\n").split("\t")
            if len(fields) >= 3:
                if not exists(fields[1]):
                    raise argparse.ArgumentError(self, 'file '+fields[1]+' doesn\'t exist')
                elif not exists(fields[2]):
                    raise argparse.ArgumentError(self, 'file '+fields[2]+' doesn\'t exist')
                else:
                    fastq_files.append(fields)
            else:
                raise argparse.ArgumentError(self, 'file '+values+' must be tab separated')

        if len(fastq_files) == 0:
            raise argparse.ArgumentError(self, 'file '+values+' has no samples')

        setattr(namespace, self.dest, fastq_files)


class VariantRecalibrationArgumentAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        message = ''

        if namespace.pp_task == 0:
            message = 'incompatible with argument -ps/--pprocess-task = 0 (hard-filtering)'

        if len(values) != 2:
            message = 'requires 2 arguments'
        else:
            if not exists(values[0]):
                message = ('first argument requires a valid file')
            try:
                int(values[1])
            except TypeError:
                message = ('second argument requires an integer')

            if int(values[1]) < 0:
                message = ('second argument requires a positive integer')

        if message:
            raise argparse.ArgumentError(self, message)
        setattr(namespace, self.dest, values)


class HardFilteringArgumentAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        message = ''
        if namespace.pp_task == 1:
            message = 'incompatible with argument -ps/--pprocess-task = 1 (variant-recalibration)'

        if message:
            raise argparse.ArgumentError(self, message)

        setattr(namespace, self.dest, values)


class SNPCallingArgumentAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        message = ''
        if namespace.known_variants is not None:
            message = 'incompatible with argument -kv/--known-variants (boostrap is required only when there are no knwon variants)'	

        if message:
            raise argparse.ArgumentError(self, message)

        setattr(namespace, self.dest, values)


# parse comand line
def parse_cmd():
    """
    return the parsed comand-line arguments
    """

    parser = VDPArgumentParser(prog="VariantDiscovery",
                               description="A workflow to perform variant discovery analysis",
                               epilog="Author:  Diego Diaz <ddiaz@dim.uchile.cl>")

    subparsers = parser.add_subparsers(help="Sub-command help")

    gatk_parser = subparsers.add_parser('gatk_snps',
                                        help='GATK SNP calling pipeline')

    gatk_parser.set_defaults(task="gatk_calling")

    gatk_parser.add_argument(
        'INPUT',
        help='tab-separated file indicanting the BAM file per sample',
        default=None,
        action=BamFilesArgumentAction)

    gatk_parser.add_argument(
        'REF_GENOME',
        help='FASTA file containing the reference genome',
        default=None,
        type=lambda x: is_valid_file(parser, x))

    gatk_group = gatk_parser.add_argument_group('pipeline arguments')

    gatk_group.add_argument(
        '-d', '--project-name',
        help='name of the project (default="VDP_gatk_snp_pipeline")',
        default='VDP_gatk_snp_pipeline',
        dest='project_name',
        metavar='STRING')

    gatk_group.add_argument(
        '-j', '--number-jobs',
        help='maximum number of active jobs (default 1)',
        type=int,
        default=1,
        dest='n_jobs',
        metavar='INT')

    gatk_group.add_argument(
        '-t', '--number-threads',
        help='maximum number of threads per sample for variant calling'
        ' (default 1)',
        type=int,
        default=1,
        dest='n_threads',
        metavar='INT')

    gatk_group.add_argument(
        '-T', '--number-joint-threads',
        help='maximum number of threads for joint genotyping (default 10)',
        type=int,
        default=10,
        dest='n_joint_threads',
        metavar='INT')

    gatk_group.add_argument(
        '-m', '--mem',
        help='maximum amount of memory en MB per sample for variant calling'
        '  (default 1000)',
        type=int,
        default=1000,
        dest='job_mem',
        metavar='INT')

    gatk_group.add_argument(
        '-M', '--joint-mem',
        help='maximum amount of memory en MB for joint genotyping'
        ' (default 10000)',
        type=int,
        default=10000,
        dest='joint_mem',
        metavar='INT')

    gatk_group.add_argument(
        '-c', '--cluster',
        help='workload manager for clusters. 1 for SLURM, '
        '2 for Condor and 2 for LFS. If this argument '
        'is not setted, then the pipe will run in local '
        'mode (Default None)',
        dest='cluster',
        choices=range(1, 4),
        default=None,
        type=int)

    gatk_group.add_argument(
        '-P', '--partition',
        help='define a cluster partition (default None)',
        default=None,
        dest='part',
        metavar='STRING')

    gatk_group.add_argument(
        '-v', '--verbose',
        help='verbose level. 0 for disabling, 1 for info,'
        ' 2 for warnings, 3 for debug purposes'
        '(default 3).',
        choices=range(0, 3),
        default=3,
        type=int,
        dest='verbose_level')

    gatk_group.add_argument(
        '-L', '--log-file',
        help='log file (default project_name.log)',
        dest='log_file',
        default=None,
        metavar='STRING')

    gatk_group.add_argument(
        '--touch',
        help='create or update input/output files only'
        ' to simulate running the pipeline. Do not run'
        ' jobs (defualt False)',
        action='store_true')

    gatk_group.add_argument(
        '--just-print',
        help='just print the pipeline instead of run it'
        ' (Useful for debugging)',
        dest='just_print',
        action='store_true')

    gatk_group.add_argument(
        '--print-graphs',
        help='generate a image of the state of the pipeline'
        ' (Useful for debugging)',
        dest='print_graph',
        action='store_true')

    gatk_group.add_argument(
        '-k', '--known-variants',
        help='VCF file with known variants',
        dest='known_variants',
        type=lambda x: is_valid_file(parser, x),
        default=None,
        metavar='FILE')

    pp_parser = subparsers.add_parser('preproc', help='Preprocessing pipeline')

    pp_parser.add_argument('INPUT',
                           help='tab-separated file indicanting FASTQ files per sample',
                           default=None,
                           action=FastqFilesArgumentAction)

    pp_parser.add_argument('REF_GENOME',
                           help='FASTA file containing the reference genome',
                           default=None,
                           type=lambda x: is_valid_file(parser, x))

    pp_parser.set_defaults(task="preprocessing")

    p_group = pp_parser.add_argument_group('pipeline arguments')

    p_group.add_argument('-d', '--project-name',
                         help='name of the project (default="VariantDiscovery_pipeline")',
                         default='VDP_preprop_pipeline',
                         dest='project_name',
                         metavar='STRING')

    p_group.add_argument('-j', '--number-jobs',
                         help='maximum number of active jobs (default 1)',
                         type=int,
                         default=1,
                         dest='n_jobs',
                         metavar='INT')

    p_group.add_argument('-t', '--number-threads',
                         help='maximum number of threads per job (default 1)',
                         type=int,
                         default=1,
                         dest='n_threads',
                         metavar='INT')

    p_group.add_argument('-m', '--mem',
                         help='maximum amount of memory en MB for each job  (default 1000)',
                         type=int,
                         default=1000,
                         dest='job_mem',
                         metavar='INT')

    p_group.add_argument('-c', '--cluster',
                         help='workload manager for clusters. 1 for SLURM, '
                         '2 for Condor and 2 for LFS. If this argument '
                         'is not setted, then the pipe will run in local '
                         'mode (Default None)',
                         dest='cluster',
                         choices=range(1, 4),
                         default=None,
                         type=int)

    p_group.add_argument('-v', '--verbose',
                         help='verbose level. 0 for disabling, 1 for info,'
                         ' 2 for warnings, 3 for debug purposes'
                         '(default 3).',
                         choices=range(0, 3),
                         default=3,
                         type=int,
                         dest='verbose_level')

    p_group.add_argument('-L', '--log-file',
                         help='log file (default project_name.log)',
                         dest='log_file',
                         default=None,
                         metavar='STRING')

    p_group.add_argument('--touch',
                         help='create or update input/output files only'
                         ' to simulate running the pipeline. Do not run'
                         ' jobs (defualt False)',
                         action='store_true')

    p_group.add_argument('--just-print',
                         help='just print the pipeline instead of run it'
                         ' (Useful for debugging)',
                         dest='just_print',
                         action='store_true')

    p_group.add_argument('--print-graphs',
                         help='generate a image of the state of the pipeline'
                         ' (Useful for debugging)',
                         dest='print_graph',
                         action='store_true')

    p_group.add_argument('-k', '--known-variants',
                         help='VCF file with known variants',
                         dest='known_variants',
                         type=lambda x: is_valid_file(parser, x),
                         default=None,
                         metavar='FILE')

    # s_group = parser.add_argument_group('SNP calling arguments')

    # s_group.add_argument('-bt', '--boostraping',
    #                      help='Number of boostraping loops for variant calling. Only available '
    #                      'when --known-variants flag is not set (default 3)',
    #                      dest='n_bst',
    #                      type=int,
    #                      default=3,
    #                      metavar='INT',
    #                      action=SNPCallingArgumentAction)

    # s_group.add_argument('-nct', '--number-cpu-threads',
    #                      help='number of CPU threads (default 1)',
    #                      dest='nct',
    #                      metavar='INT',
    #                      default=1)

    # v_group = parser.add_argument_group('VCF post processing arguments')

    # v_group.add_argument('-ps', '--pprocess-task',
    #                      help='Post processing task. 0 for hard '
    #                      'filtering and 1 for variant recalibration '
    #                      '(default 0)',
    #                      choices=range(0, 2),
    #                      default=0,
    #                      metavar='INT',
    #                      type=int,
    #                      dest='pp_task')

    # v_group.add_argument('-QD', '--QualByDepth',
    #                      help='QUAL / DEPTH. First value is for SNP '
    #                      'filtering and second for INDEL filtering '
    #                      '[hard filtering] (default 2.0 2.0)',
    #                      dest='filt_qd',
    #                      type=float,
    #                      nargs=2,
    #                      default=[2.0, 2.0],
    #                      metavar=('FLOAT', 'FLOAT'),
    #                      action=HardFilteringArgumentAction)

    # v_group.add_argument('-FS', '--FisherStrand',
    #                      help='Phred-scaled p-value using Fishers Exact Test. '
    #                      'First value is for SNP filtering and second for '
    #                      'INDEL filtering [hard filtering] (default 60.0 200.0)',
    #                      dest='filt_fs',
    #                      type=float,
    #                      nargs=2,
    #                      default=[60.0, 200.0],
    #                      metavar=('FLOAT','FLOAT'),
    #                      action=HardFilteringArgumentAction)

    # v_group.add_argument('-MQ', '--RMSMappingQuality',
    #                      help='Root Mean Square of the mapping quality of the reads '
    #                      'across all samples [hard filtering] (default 40)',
    #                      dest='filt_mq',
    #                      type=float,
    #                      default=40.0,
    #                      metavar='FLOAT',
    #                      action=HardFilteringArgumentAction)

    # v_group.add_argument('-MQRS', '--MQRankSum',
    #                      help='U-based z-approximation for mapping qualities [hard filtering]'
    #                      '(default -12.5)',
    #                      dest='filt_mqrs',
    #                      type=float,
    #                      default=-12.5,
    #                      metavar='FLOAT',
    #                      action=HardFilteringArgumentAction)

    # v_group.add_argument('-RPRS', '--ReadPosRankSum',
    #                      help='U-based z-approximation for the distance from the end of the '
    #                      'read for reads with the alternate allele. First value is for '
    #                      'SNP filtering and second for INDEL filtering [hard filtering]'
    #                      '(default -8.0 -20.0)',
    #                      dest='filt_rprs',
    #                      nargs=2,
    #                      type=float,
    #                      default=[-8.0, -20.0],
    #                      metavar=('FLOAT', 'FLOAT'),
    #                      action=HardFilteringArgumentAction)

    # v_group.add_argument('-hm', '--hapmap',
    #                      help='VCF file containing HapMap variants and its respective prior '
    #                      'probability [hard filtering] (default None None)',
    #                      dest='vr_hapmap',
    #                      default=None,
    #                      action=VariantRecalibrationArgumentAction,
    #                      nargs=2,
    #                      metavar=("STRING", "INT"))

    # v_group.add_argument('-om', '--omni',
    #                      help='VCF file containing Omni variants and its respective prior '
    #                      'probability [variant recalibration] (default None None)',
    #                      dest='vr_omni',
    #                      default=None,
    #                      action=VariantRecalibrationArgumentAction,
    #                      nargs=2,
    #                      metavar=("STRING", "INT"))

    # v_group.add_argument('-otg', '--1000-genomes',
    #                      help='VCF file containing 1000 genome variants and its respective prior '
    #                      'probability [variant recalibration] (default None None)',
    #                      dest='vr_otg',
    #                      default=None,
    #                      action=VariantRecalibrationArgumentAction,
    #                      nargs=2,
    #                      metavar=("STRING", "INT"))

    # v_group.add_argument('-ds', '--dbsnp',
    #                      help='VCF file containing dbSNP variants and its respective prior '
    #                      'probability [variant recalibration] (default None None)',
    #                      default=None,
    #                      dest='vr_dbsnp',
    #                      action=VariantRecalibrationArgumentAction,
    #                      nargs=2,
    #                      metavar=("STRING", "INT"))

    # v_group.add_argument('-mi', '--mills',
    #                      help='VCF file containing Mills variants and its respective prior '
    #                      'probability. Only for INDELs recalibration'
    #                      ' [variant recalibration] (default None None)',
    #                      default=None,
    #                      dest='vr_mills',
    #                      action=VariantRecalibrationArgumentAction,
    #                      nargs=2,
    #                      metavar=("STRING", "INT"))

    # a_group = parser.add_argument_group('Annotation arguments')

    # a_group.add_argument('-sdb', '--snpeff-db',
    #                      help='SNPEff database used to annotate variants (default REF_GENOME)',
    #                      dest='snpeff_db',
    #                      metavar='STRING',
    #                      default=None)

    # a_group.add_argument('-sr', '--snpeff-ram',
    #                      help='amount of RAM (in GB) used to run SNPEff (default 4)',
    #                      dest='snpeff_ram',
    #                      metavar='INT',
    #                      type=int,
    #                      default=4)

    # a_group.add_argument('-sbdb', '--snpeff-build-db',
    #                      help='Build SNPEff database. Only GFF or GTF annotation'
    #                      ' formats are allowed (default None)',
    #                      dest='snpeff_annot',
    #                      default=None,
    #                      type=lambda x: is_gff_or_gtf(parser, x),
    #                      metavar='GFF_FILE | GTF_FILE')

    if len(argv) == 1:
        parser.print_help()
        exit(1)

    args = parser.parse_args()

    return args
